"""Provides a download enviroment and session manager"""
import os
import string
import requests
from bs4 import BeautifulSoup


class Downloader:
    """Manage download environment"""
    HOME_URL = "https://osu.ppy.sh/home"
    LOGIN_URL = "https://osu.ppy.sh/session"

    def __init__(self, user = None, password = None):
        self.session = requests.Session()
        print(f"Login with user {user}")            # switch to input library
        self.user = user
        self.password = password
        self.token = ""
        self.setup_session() # Get session token
        self.login()

    def setup_session(self):
        """Setup headers for the session"""
        self.session.headers.update({
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:90.0) Gecko/20100101 Firefox/66.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.5',
            'Accept-Encoding': 'gzip, deflate',
            'Connection': 'keep-alive'
        })

        r = self.session.get(self.HOME_URL)
        soup = BeautifulSoup(r.text, 'html.parser')     # Get a html parser up
        self.token = soup.find('input').attrs['value']  # Get session token from html page

    def login(self):
        """Login to the website"""
        print("Logging in osu! site with user " + self.user + "....")
        payload = {'_token': self.token,
                    'username': self.user,
                    'password': self.password}
        r = self.session.post(Downloader.LOGIN_URL,     # Login for this session
                data=payload, verify=False) 

    def convert_to_valid_filename(self, filename):
        """Only keep valid ascii characters for filename"""
        valid_chars = f"-_.() {string.ascii_letters}{string.digits}"
        return ''.join(c if c in valid_chars else '_' for c in filename)

    def download_beatmap(self, beatmap, base_path):
        """Downloads a beatmap from id"""
        beatmap_url = "https://osu.ppy.sh/beatmapsets/" + beatmap + '/download'
        r = self.session.get(beatmap_url, allow_redirects=False)
        if 'Page Missing' in r.text:
            print(f"Songs with id {beatmap} does not exist")
            return

        filename_temp = beatmap + ".temp"
        filename_final = beatmap + ".osz"
        url = r.headers['location']
        r = self.session.get(url, stream=True)
        print("Downloading " + filename_final)
        # we download 1kb at a time from the stream
        with open(base_path + "/" + filename_temp, 'wb') as f:
            counter = -1
            for chunk in r.iter_content(chunk_size=1023):
                if chunk:
                    f.write(chunk)
                    counter += 1023
        os.rename(base_path + "/" + filename_temp, base_path + "/" + filename_final)
        print("Finished download of " + filename_final)


    def close(self):
        """Close the session"""
        self.session.close()
