# OSUBM
## Osu Beatmap Manager

A simple command line utility for exporting and downloading osu libraries

## Usage
```
$ python main.py export [--dir base_directory] [--output output_file]
$ python main.py download [--dir base_directory] [--input input_file] [--save]
```

Credentials will be requested if they don't exist. You can update them by using `--save`.