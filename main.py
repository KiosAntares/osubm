"""OBM: an osu beatmaps manager"""

import os
import configparser
import glob
import sys
import argparse
import getpass

from downloader import Downloader

# TODO: a proper method to get default paths (local included)
DEFAULT_DIR = '.'
DEFAULT_OUTPUT = 'songs.txt'
CRED_FILE = 'credentials.cfg'

def is_dir_path(string):
    """Check if path is a valid directory"""
    if os.path.isdir(string):
        return string
    raise NotADirectoryError(string)

def is_file_path(string):
    """Check if path is a valid file"""
    if os.path.isfile(string):
        return string
    raise FileNotFoundError(string)

def get_installed_songs(base_dir, output_file):
    """ Makes a list of songs installed in a specific directory. """
    print(f"Looking for songs in {base_dir}...")
    # Looking for all .osu files in the directory (beatmap files)
    files = glob.glob(base_dir+'/**/*.osu', recursive=True) 
    # Empty set of all beatmaps found so far
    songs = set()
    # The .osu file is basically an INI file...
    parser = configparser.ConfigParser()
    for file in files:
        with open(file, 'r', encoding='utf-8') as f:
            # However, the first line needs a dummy section and all the beatmap
            # data must be trimmed away, So we only take the first 40 lines (all metadata)
            fixed = '[DUMMY]' + os.linesep.join(f.read().split(os.linesep)[:40])
        parser.read_string(fixed)
        # Getting the beatmap set id. We can only download the set, the indivual
        #  difficulty id is useless for us and only used for ranks
        bm_id = parser["Metadata"]["BeatmapSetID"]
        print(f'Found map {file} with set id {bm_id}')
        songs.add(bm_id)

    print(f"{len(songs)} Indivual beatmap sets found:")
    for bset in songs:
        print(f" {bset}")

    with open(output_file, 'w', encoding='utf-8') as f:
        f.writelines(songs)

    print(f"Beatmaps written to file {output_file}.")

def download_songs(source_file, directory, credentials):
    """Download all the songs in a file"""
    user, password = credentials
    downloader = Downloader(user=user, password=password)
    with open(source_file, 'r', encoding='utf-8') as f:
        for line in f.readlines():
            downloader.download_beatmap(line, directory)
    downloader.close()


def parse_credentials_file(file):
    """Parse a credential file (INI)"""
    parser = configparser.ConfigParser()
    # TODO: We validated that file exists in the caller. we might still get parsing errors
    with open(file, 'r', encoding='utf-8') as f:
        parser.read(f)
    # We couldn't find the credentials, bail out
    if not (
        (user := parser['CREDENTIALS']['user'])
        and (password := parser['CREDENTIALS']['password'])):
        print("File corrupt or credentials missing. Please check or regenerate.")
        sys.exit(-1)
    return user, password

def request_credentials():
    """Asks the user for their credentials"""
    user = input("User: ")
    password = getpass.getpass("Password: ")
    return user, password

def save_credentials(credentials):
    """Saves credentials to file"""
    user, password = credentials
    parser = configparser.ConfigParser()
    parser['CREDENTIALS'] = {
        'user': user,
        'password': password
    }
    with open(CRED_FILE, 'w', encoding='utf-8') as f:
        parser.write(f)


def main(args):
    """Interactive program"""
    args = argparse.ArgumentParser(
        prog="osubm",
        description="A simple command line utility for exporting and downloading osu libraries"
    )
    args.add_argument(
        "command", 
        choices=('export', 'download'),
        help="Operation")
    args.add_argument(
        "-d", "--dir",
        type=is_dir_path,
        required=False,
        help="Base directory to search/download maps in")
    args.add_argument(
        "-o", "--output",
        type=str,
        required=False,
        help="Name of output file containing exported maps")
    args.add_argument(
        "-i", "--input",
        type=is_file_path,
        required=False,
        help="File containing maps list to install")
    args.add_argument(
        "-s", "--save",
        action='store_true',
        required=False,
        help="Whether to save (or update) credentials")
    args = args.parse_args()

    if args.command == 'export':
        base_dir = args.dir or DEFAULT_DIR
        output_file = args.output or DEFAULT_OUTPUT
        get_installed_songs(base_dir, output_file)
   
    if args.command == 'download':
        base_dir = args.dir or DEFAULT_DIR
        input_file = args.input or DEFAULT_OUTPUT
        # We validate the file existing here
        if args.save or not is_file_path(CRED_FILE):
            credentials = request_credentials()
            save_credentials(credentials)
        else: 
            credentials = parse_credentials_file(CRED_FILE)

        download_songs(input_file, base_dir, credentials)



if __name__ == '__main__':
    main(sys.argv[1:])
